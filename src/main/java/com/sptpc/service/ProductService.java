package com.sptpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sptpc.domain.Product;
import com.sptpc.persistence.ProductMapper;
//服务层
@Service
public class ProductService {	
	//com.sptc.persistence包下面
	@Autowired
	private ProductMapper productMapper;

	public List<Product> getHotProduct() {
		// TODO Auto-generated method stub
		//服务层调用持久层
		return productMapper.queryAllHotProduct();
	}

}
