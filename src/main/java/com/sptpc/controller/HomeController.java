package com.sptpc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sptpc.service.ProductService;
//控制层
@Controller
public class HomeController {
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home(Model model) {
		// 调用服务层service，获得热门产品
		model.addAttribute("hlist", productService.getHotProduct());
		return "index";
	}

}
